// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/courses');
const auth = require('../auth');

// Routing Component
const route = exp.Router();

// Post Route
route.post('/', auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data = {
		course: req.body
	};
	if (isAdmin) {
		cont.addCourse(data).then(outcome => {
			res.send(outcome)
		});
	} else {
		res.send('User not an admin.')
	}
});

// Get Route
route.get('/all', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin;
	isAdmin ? cont.getAllCourses().then(outcome => res.send(outcome)) : res.send('Unauthorized user.');
});

route.get('/active', (req, res) => {
	cont.getAllActive().then(outcome => {
		res.send(outcome);
	});
});

route.get('/:id', (req, res) => {
	let data = req.params.id;
	cont.getCourse(data).then(outcome => {
		res.send(outcome);
	});
});

// Put Route
route.put('/:courseId', auth.verify, (req, res) => {
	let params = req.params;
	let body = req.body;
	if (auth.decode(req.headers.authorization).isAdmin) {
		cont.updateCourse(params, body).then(outcome => {
			res.send(outcome);
		});
	} else {
		res.send('Unauthorized action.')
	}
});

route.put('/:courseId/setActive', (req, res) => {
	let courseId = req.params.courseId
	cont.setAsActive(courseId).then(outcome => {
		res.send(outcome);
	});
});

// Course Archive 
route.put('/:courseId/archive', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let isAdmin = auth.decode(token).isAdmin;
	let params = req.params;
	(isAdmin) ? 
	cont.archiveCourse(params).then(result => {
		res.send(result);
	})
	:
		res.send('Unauthorized user.')
})

// Delete Route
route.delete('/:courseId', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let isAdmin = auth.decode(token).isAdmin;
	let id = req.params.courseId;
	isAdmin ? cont.deleteCourse(id).then(result => res.send(result)) : res.send('Unauthorized user.')
});

// Exports
module.exports = route;
