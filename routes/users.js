// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/users');
const auth = require('../auth');

// Routing Component
const route = exp.Router();

// Post Route
route.post('/register', (req, res) => {
	let userData = req.body;
	cont.registerUser(userData).then(result => {
		res.send(result);
	});
});

route.post('/check-email', (req, res) => {
	cont.checkEmailExists(req.body).then(outcome => {
		res.send(outcome);
	})
});

route.post('/login', (req, res) => {
	let data = req.body;
	cont.loginUser(data).then(outcome => {
		res.send(outcome);
	});
});

route.post('/enroll', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let id = payload.id;
	let isAdmin = payload.isAdmin;	
	let subjectId = req.body.courseId;
	let data = {
		userId: id,
		courseId: subjectId
	}
	if (!isAdmin) {
		cont.enroll(data).then(outcome => {
			res.send(outcome);
		})
	} else {
		res.send('User is an admin. Enrollment failed.');
	}
});

// Get Route
route.get('/userId', auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	let userId = userData.id;
	cont.getProfile(userId).then(outcome => {
		res.send(outcome);
	})
});

// Update Route
route.put('/:userId/setAdmin', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let payload = auth.decode(token);
	let isAdmin = payload.isAdmin
	let userId = req.params.userId;
	(isAdmin) ? cont.setAsAdmin(userId).then(outcome => res.send(outcome)) : res.send('Unauthorized user.');
});

route.put('/:userId/setNonAdmin', auth.verify, (req, res) => {
	let token = req.headers.authorization;
	let isAdmin = auth.decode(token);
	let userId = req.params.userId
	isAdmin ? cont.setAsNonAdmin(userId).then(outcome => res.send(outcome)): res.send('Unauthorized user.');
});

// Exports
module.exports = route;