// Dependencies and Modules
const Course = require('../models/Course');

// Create Functionality
module.exports.addCourse = (info) => {
	let course = info.course
	let cName = course.name;
	let cDesc = course.description;
	let cPrice = course.price;
	let newCourse = new Course({
		name: cName,
		description: cDesc,
		price: cPrice
	});
	return newCourse.save().then((saved, err) => {
		if (saved) {
			return saved
		} else {
			return false
		}
	});
}

// Retrieve Functionality
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
};

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
};

module.exports.getCourse = (id) => {
	return Course.findById(id).then(result => {
		return result;
	});
};

// Update Functionality
module.exports.updateCourse = (course, details) => {
	let cName = details.name;
	let cDesc = details.description;
	let cPrice = details.price;
	let updatedCourse = {
		name: cName,
		description: cDesc,
		price: cPrice
	};
	let id = course.courseId;
	return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
		if (courseUpdated) {
			return courseUpdated;
		} else {
			return 'Failed to update.'
		};
	});
};

module.exports.setAsActive = (courseId) => {
	let updates = {
		isActive: true
	}
	return Course.findByIdAndUpdate(courseId, updates).then((courseActive, err) => {
		if (courseActive) {
			return courseActive;
		} else {
			return 'Update failed.'
		};
	});
};

// Archive Course
module.exports.archiveCourse = (course) => {
	let update = {
		isActive: false
	}
	let id = course.courseId;
	return Course.findByIdAndUpdate(id, update).then((archived, err) => {
		if (archived) {
			return 'Course archived.';
		} else {
			return false;
		}
	})
}

// Delete Functionality
module.exports.deleteCourse = (courseId) => {
	return Course.findById(courseId).then(course => {
		if (course === null) {
			return 'Nothing found.'
		} else {
			return course.remove().then((removedCourse, err) => {
				if (err) {
					return 'Failed to remove.'
				} else {
					return 'Successfully removed.'
				}
			});
		};
	});
};

