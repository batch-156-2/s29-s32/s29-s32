// Depenencies and Modules
const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// Create Functionalities
module.exports.registerUser = (data) => {
	let fN = data.firstName;
	let lN = data.lastName;
	let ema = data.email;
	let passW = data.password;
	let mobNo = data.mobileNo;

	let newUser = new User({
		firstName: fN,
		lastName: lN,
		email: ema,
		password: bcrypt.hashSync(passW, 10),
		mobileNo: mobNo
	});
	return newUser.save().then((user, err) => {
		if (user) {
			return user;
		} else {
			return 'Error.'
		};
	});
};

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return 'Email already exists.'
		} else {
			return 'Email available.'
		};
	});
};

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result === null) {
			return false;
		} else {
			const isMatched = bcrypt.compareSync(reqBody.password, result.password);
			if (isMatched) {
				let dataUser = result.toObject();
				return {accessToken: auth.createAccessToken(dataUser)}
			} else {
				return false;
			}
		};
	});
};

module.exports.enroll = async(data) => {
	let id = data.userId;
	let crsId = data.courseId;
	let isCourseEnrolled = false;
	let isCrsEnrolled = await User.findById(id).then(user => {
		for (let courseDsc of user.enrollments) {
			if (crsId === courseDsc.courseId) {
				isCourseEnrolled = true;
			} else {
				continue
			};
		}
		if (isCourseEnrolled === true) {
			return 'Already enrolled in the course.';
		} else {
			let isUserUpdated = User.findById(id).then(user => {
				console.log(user)
				user.enrollments.push({courseId: crsId});
				return user.save().then((saved, err) => {
					if (err) {
						return false;
					} else {
						return true;
					}
				});
			});
			let isCourseUpdated = Course.findById(crsId).then(course => {
				course.enrollees.push({userId: id});
				return course.save().then((saved, err) => {
					if (err) {
						return false;
					} else {
						return true;
					};
				});
			});
			if (isUserUpdated && isCourseUpdated) {
				return true;
			} else {
				return 'Enrollment failed.'
			};
		};
	})
	return isCrsEnrolled;
};



// Retrieve Functionalities
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(user => {
		return user;
	});
};

// Update Functionalities
module.exports.setAsAdmin = (userId) => {
	let updates = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
		if (admin) {
			return admin;
		} else {
			return 'Update failed.'
		};
	});
};

module.exports.setAsNonAdmin = (userId) => {
	let updates = {
		isAdmin: false
	}
	return User.findByIdAndUpdate(userId, updates).then((user, err) => {
		if (user) {
			return user;
		} else {
			return 'Update failed.'
		};
	});
};