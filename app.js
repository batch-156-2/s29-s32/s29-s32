// Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses');
	const userRoutes = require('./routes/users');

// Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const mu = process.env.MONGO_URL;

// Server Setup
	const app = express();
	app.use(cors());
	app.use(express.json());
	app.use(express.urlencoded({extended: true}))

// Server Routes
	app.use('/courses', courseRoutes);
	app.use('/users', userRoutes);

// Database Connect
	mongoose.connect(mu);
	const db = mongoose.connection;
	db.once('open', () => console.log(`Connected to Atlas!`));

// Server Responses
	app.get('/', (req, res) => {
		res.send('Project deployed successfully.');
	});
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});